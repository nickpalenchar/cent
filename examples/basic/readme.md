If you are in this directory, you can run `cent` and specify and .hurl/.cent files,
which will execute via `hurl`.

```
$ cent hoppscotch
{
  "method": "GET",
  "args": {},
  "data": "",
  "headers": {
    "accept": "*/*,image/webp",
    "cdn-loop": "netlify",
    "host": "echo.hoppscotch.io",
    "user-agent": "hurl/1.8.0",
    "x-app": "CENT-EXAMPLE",
    "x-country": "US",
    "x-forwarded-for": "75.197.161.253, 100.64.0.161",
    "x-forwarded-proto": "https",
    "x-nf-account-id": "5e2b91527eb7a24fb0054390",
    "x-nf-client-connection-ip": "75.197.161.253",
    "x-nf-request-id": "01GT4WC5MJDTTVCGVFPM9CQYBP",
    "x-nf-site-id": "5d797a9d-fe11-4582-8837-9986a4673158"
  },
  "path": "/",
  "isBase64Encoded": true
}
```

You can also run `cent` alone to see and optionally choose the available requests.

```
$ cent

hoppscotch    GET echo.hoppscotch.io/
```

## Only files with a .hurl or .cent extension get picked up by `cent`

Notice how there isn't, for example, this readme listed when you run `cent`.
