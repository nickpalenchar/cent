import path from "node:path";
import { execFileSync } from "node:child_process";
import fs from "node:fs";

export class CentReq {
  name: string;
  protected targetFile: string;
  protected vars: Map<string, string>
  #_shortDescription?: string;


  constructor(file: string) {
    const fullPath = path.resolve(process.cwd(), file);
    this.targetFile = fullPath;
    this.name = path.basename(file, '.hurl');
  }

  get shortDescription (): string {
    if (this.#_shortDescription !== undefined) {
      return this.#_shortDescription;
    }
    // TODO this wont work with centfiles (because metadata is planned to be on first lines)
    const contents = fs.readFileSync(this.targetFile, 'utf-8').split('\n', 1)[0];
    this.#_shortDescription = contents.length > 30 ? contents.slice(0, 27) + '...' : contents;
    return this.#_shortDescription;
  }

  withVars(vars: Map<string, string> | Record<string, string>) {
    if (!(vars instanceof Map)) {
      vars = new Map(Object.entries(vars));
    }
    this.vars = vars;
  }

  #getVarsFlags() {
    if (!this.vars) {
      return [];
    }
    const args: string[] = [];
    for (const [key, value] of this.vars.entries()) {
      args.push('--variable', `${key}=${value}`);
    }
    return args;
  }

  /** sends the request */
  send() {
    try {
      const variableArgs = this.#getVarsFlags();
      const result = execFileSync("hurl", [...variableArgs, this.targetFile], { stdio: 'pipe' });
      process.stdout.write(result);
      process.stdout.write("\n");
    } catch (e) {
      if (e.code === 'ENOENT') {
        process.stderr.write("Error: cent depends on hurl. Download hurl at http://hurl.dev\n");
      }
      process.stderr.write(e.stderr);
      process.exit(e.code ?? 1);
    }
  }
}