#!/usr/bin/env node
import minimist from 'minimist';
import chalk from 'chalk';
import { CentReq } from './CentReq';
import fs from "node:fs/promises";
import path from "node:path";
import { parseEnvFile } from './parseEnvFile';

const CENT_ENVFILE = '.centenv';

const args = minimist(process.argv.slice(2), {

});

const getRequestName = (args: minimist.ParsedArgs) => {
  if (args._.length > 0) {
    return args._[0];
  }
  return null;
}

interface AvailableReqsMeta {
  longestName: number;
}

/** Gets the available "requests", based on .hurl or .cent
 *  file extensions in the current working directory
 */
const getAvailableRequsets = async (cwd?: string): Promise<{ 
  reqs: Map<string, CentReq>, 
  env: {
    default: Map<string, string>,
    [environment: string]: Map<string, string>
  }
}> => {
  if (!cwd) {
    cwd = process.cwd()
  }
  const meta: AvailableReqsMeta = {
    longestName: -1,
  }
  const files = await fs.readdir(cwd, {
    encoding: 'utf-8',
  });
  const result: Map<string, CentReq> = new Map();
  let env;
  for (const file of files) {

    // parse the env file
    if (file == CENT_ENVFILE) {
      env = await parseEnvFile(path.resolve(cwd, file));
    }
    // lookup available hurl requests
    if (path.extname(file) === '.hurl') {
      const resolved = path.resolve(path.resolve(cwd, file));
      const centReq = new CentReq(resolved);
      result.set(centReq.name, centReq);
    }
  }
  return {
    reqs: result,
    env: env ?? { default: new Map() },
  };
}

const main = async () => {

  /// scratch
  await parseEnvFile(path.join(process.cwd(), '.centenv'));
  /// end-scratch

  const { reqs: availableReqs, env } = await getAvailableRequsets();
  const reqName = getRequestName(args);

  if (!reqName) {
    process.stdout.write(chalk.yellow("Requests available:\n"));
    for (const [key, value] of availableReqs.entries()) {
      // TODO replace padEnd with maxlength from getCommand metadata
      process.stdout.write(`${value.name.padEnd(12)}  ${value.shortDescription}\n`)
    }
    return;
  }

  const centReq = availableReqs.get(reqName)
  if (centReq) {
    // TODO: different envs besides 'default'
    centReq.withVars(env.default);
    centReq.send();
    return;
  }
  console.log('Request not found');
}

main();
