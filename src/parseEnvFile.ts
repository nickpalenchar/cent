import { createReadStream } from "node:fs";
import readline from "node:readline";

// TODO: return multiple environments
// {
//    default: {...}
      // myEnv: {...} <-- this
// }
export const parseEnvFile = async (filepath: string) => {
  const fileStream = await createReadStream(filepath);
  const rl = readline.createInterface({
    input: fileStream
  });
  const result: Map<string, string> = new Map();
  for await (let line of rl) {
    line = line.replace(/#.*$/, '')
      .replace(/^\s*/, '')
      .replace(/\s*$/, '');
    if (!line) {
      continue;
    }
    const [key, ...value] = line.split('=');
    result.set(key, value.join('='))
  }
  return { default: result };
}